include:
    - project: 'veracode-gitlab-manual/Pipeline-Templates'
      ref: main
      file: '/Veracode-Scanning/veraocde_sast_policy_scan.yml'
    - project: 'veracode-gitlab-manual/Pipeline-Templates'
      ref: main
      file: '/Veracode-Scanning/veracode_sast_sandbox_scan.yml'
    - project: 'veracode-gitlab-manual/Pipeline-Templates'
      ref: main
      file: '/Veracode-Scanning/veracode_sast_pipeline_scan.yml'
    - project: 'veracode-gitlab-manual/Pipeline-Templates'
      ref: main
      file: '/Veracode-Scanning/veracode_sca_application_scan.yml'
    - project: 'veracode-gitlab-manual/Pipeline-Templates'
      ref: main
      file: '/Veracode-Scanning/veracode_sca_docker_scan.yml'
    - project: 'veracode-gitlab-manual/Pipeline-Templates'
      ref: main
      file: '/Veracode-Scanning/veracode_dast_rescan.yml'
    - project: 'veracode-gitlab-manual/Pipeline-Templates'
      ref: main
      file: '/Veracode-Reporting/veracode_sast_policy_scan_reporting.yml'
    - project: 'veracode-gitlab-manual/Pipeline-Templates'
      ref: main
      file: '/Veracode-Reporting/veracode_sast_sandbox_scan_reporting.yml'


stages:
    - Build
    - Security_Scan
    - Scan_Reporting
    - Housekeeping
    - Deploy_Run
    - Dynamic_Analysis


Compile Application:
    image: maven:3.6.0-jdk-8
    stage: Build
    script:
      - mvn clean package
    artifacts:
      name: verademo_julian_10_build
      paths:
        - target/
      expire_in: 5 week
    cache:
      paths:
        - target/
        - .m2/repository


# veracode_sca_application_scan:
#     stage: Security_Scan
#     extends: .veracode_sca_application_scan



# veracode_sca_docker_scan:
#     stage: Security_Scan
#     extends: .veracode_sca_docker_scan
#     only:
#         - my-feature-branch
#         - main
#         - merge_requets
#         - schedules
#     except:
#         - pushes
#     variables:
#         DOCKER_IMAGE_NAME: 'juliantotzek/verademo1-tomcat'


veracode_sast_pipeline_scan:
    stage: Security_Scan
    only:
        - my-feature-branch
        - pushes
    except:
        - schedules
    extends: .veracode_sast_pipeline_scan
    variables:
        VERACODE_FILEPATH: 'target/verademo.war'
        VERACODE_POLICYNAME: 'VeraDemo_Policy'
        VERACODE_BASELINE_FILENAME: pipeline-baseline.json


# veracode_sast_sandbox_scan:
#     stage: Security_Scan
#     extends: .veracode_sast_sandbox_scan
#     only:
#         refs: 
#             - my-feature-branch
#             - schedules
#         variables: 
#             - $CI_COMMIT_BRANCH == "my-feature-branch"
#     except:
#         refs:
#             - main
#             - pushes
#     variables:
#         VERACODE_FILEPATH: target/verademo.war

# veracode_sast_policy_scan:
#     stage: Security_Scan
#     extends: .veracode_sast_policy_scan
#     only:
#         refs:
#             - main
#             - merge_requets
#             - schedules
#         variables:
#             - $CI_COMMIT_BRANCH == "main"
#     except:
#         refs:
#             - my-feature-branch
#             - pushes
#     variables:
#         VERACODE_FILEPATH: target/verademo.war


# veracode_sast_sandbox_scan_reporting:
#     stage: Scan_Reporting
#     extends: .veracode_sast_sandbox_scan_reporting
#     only:
#         refs: 
#             - my-feature-branch
#             - schedules
#         variables: 
#             - $CI_COMMIT_BRANCH == "my-feature-branch"
#     except:
#         refs:
#             - main
#             - pushes

# veracode_sast_policy_scan_reporting:
#     stage: Scan_Reporting
#     extends: .veracode_sast_policy_scan_reporting
#     only:
#         refs:
#             - main
#             - merge_requets
#             - schedules
#         variables:
#             - $CI_COMMIT_BRANCH == "main"
#     except:
#         refs:
#             - my-feature-branch
#             - pushes


# Docker Start:
#     image: docker:19.03.8-dind 
#     stage: Deploy_Run
#     only:
#         - schedules
#         - master
#     services:
#          - docker:19.03.8-dind
#     before_script:
#             - docker info
#     script:
#         - docker pull juliantotzek/verademo1-tomcat
#         - docker image history juliantotzek/verademo1-tomcat

# veracode_dast_rescan:
#     stage: Dynamic_Analysis
#     extends: .veracode_dast_rescan
#     only:
#         - main
#         - my-feature-branch
#         - merge_requets
#         - schedules
#     except:
#         - pushes